package fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.entities.Location;
import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.exceptions.DatabaseException;
import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.services.LocationService;

@RestController
@RequestMapping(value = "/rest/locations")
@CrossOrigin(origins = "*")
public class LocationController {
	
	@Autowired
	private LocationService locationService;
	
	@GetMapping
	public ResponseEntity<?> listLocations() {
		try {
			List<Location> locations = this.locationService.list();
			return new ResponseEntity<List<Location>>(locations, HttpStatus.OK);
		} catch (DatabaseException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getLocationById(@PathVariable Integer id) {
		try {
			Location location = this.locationService.get(id);
			return new ResponseEntity<Location>(location,  HttpStatus.OK);
		} catch (DatabaseException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
