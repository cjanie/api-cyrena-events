package fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.repositories;

import org.springframework.data.repository.CrudRepository;

import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.entities.Event;

public interface EventRepository extends CrudRepository<Event, Integer> {

}
