package fr.fondespierre.beweb.java.ee.spring.apicyrenaevents;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCyrenaEventsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCyrenaEventsApplication.class, args);
	}

}
