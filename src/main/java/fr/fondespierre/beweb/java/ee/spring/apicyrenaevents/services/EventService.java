package fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.entities.Event;
import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.exceptions.DatabaseException;
import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.repositories.EventRepository;

@Service
public class EventService {

	@Autowired
	private EventRepository eventRepository;
	
	public List<Event> list() throws DatabaseException {
		List<Event> events = new ArrayList<>();
		this.eventRepository.findAll().forEach(event -> {
			events.add(event);
		});
		return events;
	}
	
	public Event get(Integer id) throws DatabaseException {
		return this.eventRepository.findById(id).get();
	}
}
