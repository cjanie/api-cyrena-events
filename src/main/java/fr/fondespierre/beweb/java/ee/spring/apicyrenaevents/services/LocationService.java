package fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.entities.Location;
import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.exceptions.DatabaseException;
import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.repositories.LocationRepository;

@Service
public class LocationService {
	
	@Autowired
	private LocationRepository locationRepository;
	
	public List<Location> list() throws DatabaseException {
		List<Location> locations = new ArrayList<>();
		this.locationRepository.findAll().forEach(location -> {
			locations.add(location);
			});
		return locations;
	}
	
	public Location get(Integer id) throws DatabaseException {
		return this.locationRepository.findById(id).get();
	}

}
