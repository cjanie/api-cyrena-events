package fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.entities.Event;
import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.exceptions.DatabaseException;
import fr.fondespierre.beweb.java.ee.spring.apicyrenaevents.services.EventService;

@RestController
@RequestMapping(value = "rest/events")
@CrossOrigin(origins = "*")
public class EventController {
	
	@Autowired
	private EventService eventService;
	
	@GetMapping
	public ResponseEntity<?> listEvents() {
		try {
			List<Event> events = this.eventService.list();
			return new ResponseEntity<List<Event>>(events, HttpStatus.OK);
		} catch (DatabaseException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getEventById(@PathVariable Integer id) {
		try {
			Event event = this.eventService.get(id);
			return new ResponseEntity<Event>(event, HttpStatus.OK);
		} catch (DatabaseException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
